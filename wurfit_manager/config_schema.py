from flask import current_app

import json

def config_schema():
    with current_app.open_resource('static/config_schema.json') as f:
        return json.load(f)
