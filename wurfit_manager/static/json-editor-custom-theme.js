JSONEditor.defaults.themes.custom = JSONEditor.AbstractTheme.extend({
  getHeader: function(text) {
    var el = document.createElement('span');

    el.setAttribute("style",'display:none;');
    // if(typeof text === "string") {
    //   el.textContent = text;
    // }
    // else {
    //   el.appendChild(text);
    // }

    return el;
  },
  getSelectInput: function(options) {
    var el = this._super(options);
    el.classList.add("form-control");
    return el;
  },
  setGridColumnSize: function(el, size) {
    el.classList.add("col-md-" + size);
  },
  afterInputReady: function(input) {
    if (input.controlgroup) return;
    input.controlgroup = this.closest(input, ".form-group");
    if (this.closest(input, ".compact")) {
      input.controlgroup.style.marginBottom = 0;
    }
  },
  getTextareaInput: function() {
    var el = document.createElement("textarea");
    el.classList.add("form-control");
    return el;
  },
  getRangeInput: function(min, max, step) {
    return this._super(min, max, step);
  },
  getFormInputField: function(type) {
    var el = this._super(type);
    if (type !== "checkbox") {
      el.classList.add("form-control");
    }
    return el;
  },
  getCheckbox: function() {
    var el = this.getFormInputField('checkbox');
    return el;
  },
  getCheckboxLabel: function(text) {
    var el = this.getFormInputLabel(text);
    return el;
  },
  getFormControl: function(label, input, description) {
    var group = document.createElement("div");

    if (label && input.type === "checkbox") {
      group.classList.add("control", "checkbox");
      input.classList.add("control-input");
      label.classList.add("control-label");
      group.appendChild(input);
      group.appendChild(document.createTextNode (" "));
      group.appendChild(label);
    } else {
      group.classList.add("form-group");
      if (label) {
        label.classList.add("form-control-label");
        group.appendChild(label);
      }
      group.appendChild(input);
    }

    if (description) group.appendChild(description);

    return group;
  },
  getIndentedPanel: function() {
    var el = document.createElement("div");
    // el.classList.add('container');
    return el;
  },
  getFormInputDescription: function(text) {
    var el = document.createElement("p");
    el.classList.add('form-text');
    el.innerHTML = text;
    return el;
  },
  getHeaderButtonHolder: function() {
    var el = this.getButtonHolder();
    el.style.marginLeft = "10px";
    return el;
  },
  getButtonHolder: function() {
    var el = document.createElement("div");
    el.classList.add("btn-group");
    return el;
  },
  getButton: function(text, icon, title) {
    var el = this._super(text, icon, title);
    var style = "btn-secondary"
    if (title.toLowerCase().includes("add"))
    {
      style = "btn-success"
    }
    else if (title.toLowerCase().includes("delete"))
    {
      style = "btn-danger"
    }
    el.classList.add("btn", 'btn-sm', style);
    el.style.margin = "10px";
    return el;
  },
  getTable: function() {
    var el = document.createElement("table");
    el.classList.add("table-bordered", "table-sm");
    el.style.width = "auto";
    el.style.maxWidth = "none";
    return el;
  },

  addInputError: function(input, text) {
    if (!input.controlgroup) return;
    input.controlgroup.classList.add("has-error");
    if (!input.errmsg) {
      input.errmsg = document.createElement("p");
      input.errmsg.classList.add("form-text", "errormsg");
      input.controlgroup.appendChild(input.errmsg);
    } else {
      input.errmsg.style.display = "";
    }

    input.errmsg.textContent = text;
  },
  removeInputError: function(input) {
    if (!input.errmsg) return;
    input.errmsg.style.display = "none";
    input.controlgroup.classList.remove('has-error');
  },
  getTabHolder: function(propertyName) {
    var el = document.createElement("div");
    var pName = (typeof propertyName === 'undefined')? "" : propertyName;
    el.innerHTML = "<div class='col-md-2' id='" + pName + "'><ul class='nav flex-column nav-pills'></ul></div><div class='tab-content col-md-10' id='" + pName + "'></div>";
    el.classList.add("row");
    return el;
  },
  addTab: function(holder, tab) {
    holder.children[0].children[0].appendChild(tab);
  },
  getTopTabHolder: function(propertyName) {
    var pName = (typeof propertyName === 'undefined')? "" : propertyName;
    var el = document.createElement('div');
    el.innerHTML = "<ul class='nav nav-tabs' id='" + pName + "'></ul><div class='card-body tab-content' id='" + pName + "'></div>";
    return el;
  },
  getTab: function(text,tabId) {
    var liel = document.createElement('li');
    liel.classList.add('nav-item');
    var ael = document.createElement("a");
    ael.classList.add("nav-link");
    ael.setAttribute("style",'padding:10px;');
    ael.setAttribute("href", "#" + tabId);
    ael.setAttribute('data-toggle', 'tab');
    ael.appendChild(text);
    liel.appendChild(ael);
    return liel;
  },
  getTopTab: function(text, tabId) {
    var el = document.createElement('li');
    el.classList.add('nav-item');
    var a = document.createElement('a');
    a.classList.add('nav-link');
    a.setAttribute('href','#'+tabId);
    a.setAttribute('data-toggle', 'tab');
    a.appendChild(text);
    el.appendChild(a);
    return el;
  },
  getTabContent: function() {
    var el = document.createElement('div');
    el.classList.add('tab-pane');
    el.setAttribute('role', 'tabpanel');
    return el;
  },
  getTopTabContent: function() {
    var el = document.createElement('div');
    el.classList.add('tab-pane');
    el.setAttribute('role', 'tabpanel');
    return el;
  },
  markTabActive: function(row) {
    row.tab.classList.add('active');
    row.container.classList.add('active');
  },
  markTabInactive: function(row) {
    row.tab.classList.remove('active');
    row.container.classList.remove('active');
  },
  getProgressBar: function() {
    var min = 0,
      max = 100,
      start = 0;

    var container = document.createElement("div");
    container.classList.add("progress");

    var bar = document.createElement("div");
    bar.classList.add("progress-bar");
    bar.setAttribute("role", "progressbar");
    bar.setAttribute("aria-valuenow", start);
    bar.setAttribute("aria-valuemin", min);
    bar.setAttribute("aria-valuenax", max);
    bar.innerHTML = start + "%";
    container.appendChild(bar);

    return container;
  },
  updateProgressBar: function(progressBar, progress) {
    if (!progressBar) return;

    var bar = progressBar.firstChild;
    var percentage = progress + "%";
    bar.setAttribute("aria-valuenow", progress);
    bar.style.width = percentage;
    bar.innerHTML = percentage;
  },
  updateProgressBarUnknown: function(progressBar) {
    if (!progressBar) return;

    var bar = progressBar.firstChild;
    progressBar.classList.add('progress', 'progress-striped', 'active');
    bar.removeAttribute("aria-valuenow");
    bar.style.width = "100%";
    bar.innerHTML = "";
  },
  getInputGroup: function(input, buttons) {
    if (!input) return;

    var inputGroupContainer = document.createElement('div');
    inputGroupContainer.classList.add('input-group');
    inputGroupContainer.appendChild(input);

    var inputGroup = document.createElement('div');
    inputGroup.classList.add('input-group-btn');
    inputGroupContainer.appendChild(inputGroup);

    for(var i=0;i<buttons.length;i++) {
      inputGroup.appendChild(buttons[i]);
    }

    return inputGroupContainer;
  }
});
