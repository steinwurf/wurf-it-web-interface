import requests


class Rest(object):

    def __init__(self):
        self.url = None

    def init_app(self, app):
        # url = 'http://{WURF_IT_REST_IP}:{WURF_IT_REST_PORT}'.format(**app.config)
        url = 'http://{}:{}'.format(
            app.config['WURF_IT_REST_IP'],
            app.config['WURF_IT_REST_PORT'])
        self.url = url

    def is_available(self):
        try:
            return requests.get('{}/endpoints'.format(self.url)).ok
        except requests.exceptions.RequestException as e:
            return False

    def get_servers(self):
        return requests.get('{}/servers'.format(self.url))

    def get_server(self, id):
        return requests.get('{}/servers/{}'.format(self.url, id))

    def start_server(self, request):
        return requests.post('{}/servers'.format(self.url), json=request)

    def stop_server(self, id):
        return requests.delete('{}/servers/{}'.format(self.url, id))

global rest
rest = Rest()
