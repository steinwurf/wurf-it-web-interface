from flask import Blueprint, request, flash, redirect, url_for
from .utils import base_render_template
from .forms import ServerForm
from .files import file_list
from .interfaces import interface_list
from .models import Server, db

import os
import json

servers = Blueprint('servers', __name__, template_folder='templates')

@servers.route('/servers/create', methods=['GET', 'POST'], defaults={'id': None})
@servers.route('/servers/edit/<int:id>', methods=['GET', 'POST'])
def create(id=None):
    server = Server.query.get(id) if id else None
    form = ServerForm(obj=server, original_name=server.name if server else None)
    if form.validate_on_submit():
        name = form.name.data
        config_string = form.config_string.data
        auto_start = form.auto_start.data
        if server is not None:
            if server.name != name or server.config != json.loads(config_string):
                server.stop()
            server.name = name
            server.config_string = config_string
            server.auto_start = auto_start
        else:
            server = Server(
                name=name,
                config_string=config_string,
                auto_start=auto_start)
            db.session.add(server)

        db.session.commit()
        flash(('{} saved.' if id else '{} created.').format(server.name))
        return redirect(url_for('servers.detail', id=server.id))

    return base_render_template(
        'create.html',
        form=form,
        files=file_list(),
        interfaces=interface_list(),
        server=server)

@servers.route('/servers/<int:id>', methods=['GET'])
def detail(id):
    server = Server.query.get(id)
    return base_render_template('server.html', server=server)

@servers.route('/servers/<int:id>/start', methods=['GET'])
def start(id):
    server = Server.query.get(id)
    result, code = server.start()
    if code != 201:
        flash(result, 'danger')
    else:
        db.session.commit()
        flash('{} started.'.format(server.name), 'success')
    next = request.args.get('next', default = url_for('index'))
    return redirect(next)

@servers.route('/servers/<int:id>/stop', methods=['GET'])
def stop(id):
    server = Server.query.get(id)
    server.stop()
    db.session.commit()
    flash('{} stopped.'.format(server.name), 'info')
    next = request.args.get('next', default = url_for('index'))
    return redirect(next)

@servers.route('/servers/<int:id>/delete', methods=['GET'])
def delete(id):
    server = Server.query.get(id)
    server.stop()
    flash('{} deleted.'.format(server.name), 'info')
    Server.query.filter(Server.id == id).delete()
    db.session.commit()
    return redirect(url_for('index'))