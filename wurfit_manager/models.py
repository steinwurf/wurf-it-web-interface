from flask_sqlalchemy import SQLAlchemy
from flask import current_app as app

import datetime
import requests
import json

from .rest import rest

db = SQLAlchemy()

class Server(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    config_string = db.Column(db.String(2048), nullable=False)
    auto_start = db.Column(db.Boolean, nullable=False, default=True)
    rest_id = db.Column(db.String(80), unique=True, nullable=True)
    last_updated = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    created = db.Column(db.DateTime, default=datetime.datetime.now)

    def start(self):
        try:
            response = rest.start_server(self.request)
        except requests.exceptions.RequestException as e:
            print(e)
            return "Connection Error", 0
        if response.status_code == 409: #already started
            print("Server '{}' was already started".format(response.content))
            self.rest_id = response.content.decode('utf-8')
            return response.content.decode('utf-8'), 201

        if response.ok:
            self.rest_id = response.content.decode('utf-8')

        return response.content.decode('utf-8'), response.status_code

    def stop(self):
        if self.rest_id is None:
            return "Already stopped", 0
        try:
            response = rest.stop_server(self.rest_id)
        except requests.exceptions.RequestException as e:
            print(e)
            return "Connection Error", 0
        finally:
            self.rest_id = None

        return response, response.status_code

    @property
    def config(self):
        return json.loads(self.config_string)

    @property
    def request(self):
        return { 'name': self.name, 'config': self.config }

    @property
    def protocol(self):
        return self.config.get('protocol', {'type': 'invalid'})

    @property
    def content(self):
        return self.config.get('content', [])

    @property
    def running(self):
        if self.rest_id is None:
            return False
        try:
            response = rest.get_server(self.rest_id)
            ok = response.ok
        except requests.exceptions.RequestException as e:
            ok = False
        if not ok:
            self.rest_id = None
            db.session.commit()

        return self.rest_id is not None

    @property
    def stats(self):
        if self.rest_id is None:
            return { }
        try:
            response = rest.get_server(self.rest_id)
        except requests.exceptions.RequestException as e:
            print(e)
            return { }
        return response.json() if response.ok else { }

    @property
    def stats_string(self):
        return json.dumps(self.stats)

    @property
    def rest(self):
        return Rest('http://{}:{}'.format(
            app.config['WURF_IT_REST_IP'],
            app.config['WURF_IT_REST_PORT']))

    def __repr__(self):
        return '<Server %r>' % self.name
