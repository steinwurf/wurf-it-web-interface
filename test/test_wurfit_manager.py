import os
import json
import re
import uuid
from xml.dom import minidom

import pytest
from wurfit_manager.config import Config
import wurfit_manager

@pytest.fixture
def client(tmpdir):
    settings_override = {
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI':
            'sqlite:///' + os.path.join(tmpdir.strpath, 'app.db')
    }
    app = wurfit_manager.create_app(settings_override)
    client = app.test_client()

    with app.app_context():
        wurfit_manager.init_db()

    return client

def create_server(client, name):
    rv = client.get('/servers/create')
    m = re.search(b'<input.*id="csrf_token".*value="(.*?)".*>', rv.data)
    assert m is not None
    csrf_token = m.group(1).decode('utf-8')

    # login(client, wurfit_manager.app.config['USERNAME'], flaskr.app.config['PASSWORD'])
    rv = client.post('/servers/create', follow_redirects=True, data={
        'csrf_token': csrf_token,
        'name': name,
        'root[protocol][type]': "tcp",
        'root[protocol][port]': 32211,
        'root[protocol][interface]': "127.0.0.1",
        'root[content][0][type]': 'json',
        'root[content][0][timeout_interval]': 0,
        'config_string': json.dumps({
            "protocol": {
                "type": "tcp",
                "port": 32211,
                "interface": "127.0.0.1"
            },
            "content": [
                {
                "type": "json",
                "timeout_interval": 0
                }
            ]
        })})


def test_servers(client):
    """Test that servers work."""

    # Check no servers exists
    rv = client.get('/')
    output = rv.data.decode('utf-8')
    assert 'No servers here yet' in output

    # Create server 1
    server1_name = str(uuid.uuid4())
    create_server(client, server1_name)

    # Check server 1 exists
    rv = client.get('/')
    output = rv.data.decode('utf-8')
    assert 'No servers here yet' not in output
    assert server1_name in output

    # Create server 2
    server2_name = str(uuid.uuid4())
    create_server(client, server2_name)

    # Check server 2 exists
    rv = client.get('/')
    output = rv.data.decode('utf-8')
    assert 'No servers here yet' not in output
    assert server1_name in output
    assert server2_name in output

    # Delete server 1
    rv = client.get('/servers/1/delete')

    # Check server 1 is removed
    rv = client.get('/')
    output = rv.data.decode('utf-8')
    assert 'No servers here yet' not in output
    assert '{} deleted.'.format(server1_name) in output
    assert server2_name in output

    # Check that server 1 is all gone (including flash)
    rv = client.get('/')
    output = rv.data.decode('utf-8')
    assert 'No servers here yet' not in output
    assert server1_name not in output
    assert server2_name in output
